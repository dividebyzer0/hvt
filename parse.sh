#!/bin/bash

# This could probably be shortened with some
# Bash-fu to one or two vars. Haven't had
# the time to play with it yet.
DNS_IP=10.10.20.3
LOCALHOST_IP=10.10.20.174
GR_IP=10.10.20.157
GATEWAY_IP=10.10.20.1
MS_IP=10.10.20.5
HV_IP=10.10.20.10

while true inotifywait -q -e modify /data/suricata/log/eve.json >/dev/null; do

	# super hacky janky shit
	cat /data/suricata/log/eve.json | grep '{"timestamp"' | tail -1 > last.json

	# snip the ip
	src_ip="$(cat last.json | jq  .src_ip | tr -d '"')"

	if [ "$src_ip" != "$DNS_IP" ] && [ "$src_ip" != "$LOCALHOST_IP" ] && [ "$src_ip" != "$GR_IP" ] && \
	[ "$src_ip" != "$GATEWAY_IP" ] && [ "$src_ip" != "$MS_IP" ] &&  [ "$src_ip" != "$HV_IP" ]
	then
		echo "Attack from:" $src_ip
		# pass the ip to local server for geolocation
		curl -s http://10.10.20.3:5000/${src_ip} -o geoloc.json

		# snip lat/lon for obtained json
		iplat="$(cat geoloc.json | jq --raw-output '.Data.Location.Latitude')"
		iplon="$(cat geoloc.json | jq --raw-output '.Data.Location.Longitude')"

		# push intel to file for js
		echo $iplat,$iplon > geoloc.txt
		echo $src_ip > src_ip.txt
	else
		:
	fi
done
